var colorArray = [
    '#e1f5fd',
    '#81d4fa',
    '#29b5f5',
    '#03a9f4',
    '#0288d1',
    '#01579b'
];

var tabArray = [
    {
        title: 'Tab 1',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel interdum orci. Praesent egestas, massa non rutrum vestibulum, est ipsum malesuada ante, in porta nibh velit scelerisque quam. Ut vel molestie ligula, ac accumsan nisi. Nulla rhoncus facilisis consequat. Nunc dignissim lobortis tortor sit amet sollicitudin.'
    },
    {
        title: 'Tab 2',
        content: 'Nullam in ipsum magna. Maecenas placerat metus arcu, nec posuere nisl feugiat convallis. Ut at porta enim. Integer congue nisl vitae metus ultricies condimentum. Cras vel auctor arcu. Cras volutpat lectus porta eros sagittis commodo. Maecenas sit amet ultrices quam. Sed vestibulum posuere nulla sit amet congue.'
    },
    {
        title: 'Tab 3',
        content: 'Donec rutrum nunc elementum nisi elementum, eu pharetra augue tincidunt. Suspendisse quis mi finibus, euismod elit a, tempor dui. Aliquam in risus sit amet mi blandit elementum in ac mauris. Aliquam sapien risus, venenatis a porttitor eget, facilisis eget diam. Nullam laoreet elementum sapien, et eleifend augue tristique sed.'
    },
    {
        title: 'Tab 4',
        content: 'Vivamus id tellus justo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent dictum, tortor sed aliquam ultrices, ex sem dapibus mi, quis facilisis tellus leo vel nulla. Nam scelerisque gravida euismod. Sed accumsan id tellus ut elementum. Sed fringilla semper faucibus.'
    },
    {
        title: 'Tab 5',
        content: 'Phasellus porta, urna id porttitor interdum, nulla ante ornare mi, sed facilisis eros massa sit amet nisl. Nam imperdiet nisi quis dolor sagittis facilisis. Donec bibendum laoreet lacus nec mollis. Sed in elit nec justo fermentum accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'
    },
    {
        title: 'Tab 6',
        content: 'Pellentesque lobortis nulla et felis suscipit pulvinar. Donec posuere quam et interdum dapibus. Cras pulvinar nulla sed nunc ornare, nec lobortis eros maximus. Suspendisse potenti. Duis pulvinar vel arcu et hendrerit. Aenean accumsan ex non justo convallis scelerisque. Etiam justo elit, dapibus accumsan tristique sed, convallis nec tellus.'
    }
]

fillTabsContainer(tabArray);
changeBGColor(colorArray);
initListeners(colorArray, tabArray);

function fillTabsContainer(tArray) {
    var tabItem;
    var contentTabItem;
    var tabSelector = document.getElementsByClassName('JT-tab-selector');
    tArray.forEach((element, index) => {
        tabItem = document.createElement("div");
        tabItem.id = "JT-tab-" + index;
        tabItem.classList.add("JT-tab-selector-item");
        contentTabItem = document.createTextNode(element.title);
        tabItem.appendChild(contentTabItem);
        tabSelector.item(0).appendChild(tabItem);
    });
}

function initListeners(cArray, tabs) {
    var cols = document.getElementsByClassName('JT-tab-selector-item');
    var tabExpanded;
    var titleTabExpanded;
    var contentTabExpanded;
    var closeTabExpanded;
    var expandedContainer;
    var firstContainer;
    closeTabExpanded = document.getElementsByName('JT-expanded-close');
    closeTabExpanded.item(0).classList.add('JT-hidden');
    closeTabExpanded.item(0).addEventListener('click', function () {
        tabExpanded = document.getElementsByName('JT-tab-expanded');
        closeTabExpanded = document.getElementsByName('JT-expanded-close');
        titleTabExpanded = document.getElementsByName('JT-expanded-title');
        contentTabExpanded = document.getElementsByName('JT-expanded-content');
        tabExpanded.item(0).classList.add('JT-collapsed');

        setTimeout(() => {
            var tabExpanded = document.getElementsByName('JT-tab-expanded');
            //tabExpanded.item(0).classList.add('JT-hidden');
            tabExpanded.item(0).classList.remove('JT-expanded');
            tabExpanded.item(0).classList.remove('JT-collapsed');

            /* var expandedContainer = document.getElementsByName('JT-expanded-container');
            expandedContainer.item(0).classList.remove('JT-appear');
            expandedContainer.item(0).classList.add('JT-desappear'); */
            tabExpanded.item(0).classList.add('JT-hidden');
            var closeTabExpanded = document.getElementsByName('JT-expanded-close');
            var titleTabExpanded = document.getElementsByName('JT-expanded-title');
            var contentTabExpanded = document.getElementsByName('JT-expanded-content');
            closeTabExpanded.item(0).style.display = "none";
            titleTabExpanded.item(0).innerHTML = ' ';
            contentTabExpanded.item(0).innerHTML = ' ';
            /* setTimeout(() => {
                var tabExpanded = document.getElementsByName('JT-tab-expanded');
                tabExpanded.item(0).classList.add('JT-hidden');
                var closeTabExpanded = document.getElementsByName('JT-expanded-close');
                var titleTabExpanded = document.getElementsByName('JT-expanded-title');
                var contentTabExpanded = document.getElementsByName('JT-expanded-content');
                closeTabExpanded.item(0).style.display = "none";
                titleTabExpanded.item(0).innerHTML = ' ';
                contentTabExpanded.item(0).innerHTML = ' ';
                var expandedContainer = document.getElementsByName('JT-expanded-container');
                expandedContainer.item(0).classList.remove('JT-desappear');
            }, 500); */
        }, 500);
    });

    window.addEventListener('scroll', (event) => {
        /* console.log("it's scrolling"); */
        expandedContainer = document.getElementsByName('JT-expanded-container');
        firstContainer = document.getElementsByClassName("JT-container");
        tabExpanded = document.getElementsByName('JT-tab-expanded');
        expandedContainer.item(0).style.top = firstContainer.item(0).getBoundingClientRect().y + "px";
        tabExpanded.item(0).style.top = firstContainer.item(0).getBoundingClientRect().y + "px";
    });

    for (var i = 0; i < cols.length; i++) {
        cols[i].addEventListener('click', function (event) {
            firstContainer = document.getElementsByClassName("JT-container");
            console.log(firstContainer.item(0).getBoundingClientRect());
            //console.log(event.srcElement.id.charAt(event.srcElement.id.length - 1));

            tabExpanded = document.getElementsByName('JT-tab-expanded');
            tabExpanded.item(0).style.top = firstContainer.item(0).getBoundingClientRect().y + "px";
            tabExpanded.item(0).style.left = firstContainer.item(0).getBoundingClientRect().x + "px";
            tabExpanded.item(0).style.height = firstContainer.item(0).clientHeight + "px";
            tabExpanded.item(0).style.width = firstContainer.item(0).clientWidth + "px";
            tabExpanded.item(0).style.margin = '0px';

            expandedContainer = document.getElementsByName('JT-expanded-container');
            expandedContainer.item(0).style.top = firstContainer.item(0).getBoundingClientRect().y + "px";
            expandedContainer.item(0).style.left = firstContainer.item(0).getBoundingClientRect().x + "px";
            expandedContainer.item(0).style.height = firstContainer.item(0).clientHeight + "px";
            expandedContainer.item(0).style.width = firstContainer.item(0).clientWidth + "px";
            expandedContainer.item(0).style.margin = '0px';

            closeTabExpanded = document.getElementsByName('JT-expanded-close');
            closeTabExpanded.item(0).style.display = "none";

            tabExpanded.item(0).classList.remove('JT-hidden');
            tabExpanded.item(0).style.backgroundColor = cArray[event.srcElement.id.charAt(event.srcElement.id.length - 1) % 6];
            tabExpanded.item(0).classList.add('JT-expanded');
            setTimeout(() => {
                titleTabExpanded = document.getElementsByName('JT-expanded-title');
                titleTabExpanded.item(0).innerHTML = tabs[event.srcElement.id.charAt(event.srcElement.id.length - 1)].title;
                contentTabExpanded = document.getElementsByName('JT-expanded-content');
                contentTabExpanded.item(0).innerHTML = tabs[event.srcElement.id.charAt(event.srcElement.id.length - 1)].content;
                closeTabExpanded = document.getElementsByName('JT-expanded-close');
                closeTabExpanded.item(0).style.display = "flex";
                expandedContainer = document.getElementsByName('JT-expanded-container');
                expandedContainer.item(0).classList.add('JT-appear');
            }, 500);
        });
    }
}

function changeBGColor(cArray) {
    var cols = document.getElementsByClassName('JT-tab-selector-item');
    for (var i = 0; i < cols.length; i++) {
        cols[i].style.backgroundColor = cArray[i % 6];
        if (i % 6 < 3) {
            cols[i].style.color = '#000';
        }
    }
}