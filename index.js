var MongoClient = require('mongodb').MongoClient;
var mongo_db = "mongodb://127.0.0.1/";
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//enable CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", req.headers.origin);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//define base resources
app.use(express.static(__dirname + '/app'));
app.use(express.static(__dirname + '/node_modules'));

app.get('/', function (req, res) {
    res.sendFile('/index.html');
});

//GET /spDomiciliaciones?accountId=AH00000011&status=ALL
app.get('/spDomiciliaciones', function (req, res) {
    if (req.query.status == "ALL") {
        getAllDoc("domis", { "accountId": req.query.accountId }, function (result) {
            res.send({ "data": result });
        }, function (err) {
            res.status(500);
            res.send(err);
        });
    }
});

//POST /spRelateDomiciliacion
app.post('/spRelateDomiciliacion', function (req, res) {
    var data = req.body;
    updateDoc("domis", { reference: { $in: [data.reference] } }, { $set: { pocketId: req.body.pocketId } }, function (result) {
        res.send(result);
    }, function (err) {
        res.status(500);
        res.send(err);
    });
});

//POST /spUnrelateDomiciliacion
app.post('/spUnrelateDomiciliacion', function (req, res) {
    var data = req.body;
    updateDoc("domis", { reference: { $in: [data.reference] } }, { $set: { pocketId: "NA" } }, function (result) {
        res.send(result);
    }, function (err) {
        res.status(500);
        res.send(err);
    });
});

//POST /spMultipleUnrelateDomiciliacion
app.post('/spMultipleUnrelateDomiciliacion', function (req, res) {
    var query = { reference: { $in: [] } };
    /* var data = req.body;
    var index = 0;
    var q = "";
    while(true){
        q="dataList["+index+"][reference]";
        if(data[q]){
            query.reference.$in.push(data[q]);
        } else{
            break;
        }
        index++;
    } */

    req.body.dataList.forEach(function(element) {
        query.reference.$in.push(element.reference);
    });

    updateDoc("domis", query, { $set: { pocketId: "NA" } }, function (result) {
        res.send(result);
    }, function (err) {
        res.status(500);
        res.send(err);
    });
});

//POST /spMultipleRelateDomiciliacion
app.post('/spMultipleRelateDomiciliacion', function (req, res) {
    var query = { reference: { $in: [] } };

    /* var data = req.body;
    var index = 0;
    var q = "";
    while(true){
        q="dataList["+index+"][reference]";
        if(data[q]){
            query.reference.$in.push(data[q]);
        } else{
            break;
        }
        index++;
    }
    q = "dataList[0][pocketId]";
    var newValues = { $set: { pocketId: data[q] } }; */

    req.body.dataList.forEach(function(element) {
        query.reference.$in.push(element.reference);
    });
    var newValues = { $set: { pocketId: req.body.dataList[0].pocketId } };

    updateDoc("domis", query, newValues, function (result) {
        res.send(result);
    }, function (err) {
        res.status(500);
        res.send(err);
    });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

var updateDocuments = function (db, nameCollection, query, newValues, success, error) {
    var collection = db.collection(nameCollection);
    collection.updateMany(query, newValues, function (err, result) {
        if (result) {
            success(result);
        }
        if (err) {
            error(err);
        }
    });
}

var findDocuments = function (db, nameCollection, query, success, error) {
    var collection = db.collection(nameCollection);
    collection.find(query).toArray(function (err, docs) {
        if (docs) {
            success(docs);
        }
        if (err) {
            error(err);
        }
    });
}

var updateDoc = function (nameCollection, query, newValues, success, error) {
    MongoClient.connect(mongo_db, function (err, db) {
        if (db) {
            var dbo = db.db('spd');
            updateDocuments(dbo, nameCollection, query, newValues, function (result) {
                console.log(result);
                db.close();
                success(result);
            }, error);
        } else {
            error(err);
        }
    });
}

var getAllDoc = function (nameCollection, query, success, error) {
    MongoClient.connect(mongo_db, function (err, db) {
        if (db) {
            var dbo = db.db('spd');
            findDocuments(dbo, nameCollection, query, function (result) {
                documents = result;
                db.close();
                success(documents);
            }, error);
        } 
        if(err) {
            error(err);
        }
        /*  */
    });
}